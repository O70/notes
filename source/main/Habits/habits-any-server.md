---
title: Any Server
date: 2019-10-16 11:19:00
---

``` sh
$ python -m SimpleHTTPServer
```

``` sh
$ python3 -m http.server -d ./build/reports/tests/test/
```

``` sh
$ npx http-server
```
