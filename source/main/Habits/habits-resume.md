---
title: 古籍站點
date: 2019-01-21 08:00:00
toc: true
categories:
- Life
tags:
- resume
---

## 个人信息

- 徐雄兵/男/1990
- Email: thraex@aliyun.com
- 手机: `18310413170`
- GitHub: [O70](https://github.com/O70)
- 工作经验: `6年+`
- 现住城市: 北京

## 教育经历

- 学校名称: **江西农业大学**
- 就读时间: **2008.09-2012.07**
- 专业: **软件工程**
- 学历: **本科**

## 工作经历

- 北京东晨联创科技股份有限公司(2014.04-至今)

- 联想中望系统服务有限公司(2011.10-2012.07)

## 项目经历

- **综合办公管理平台**
  - 项目时间: 2018.06-至今
  - 项目描述:
    > 对以往所构建的所有内部及其他平台的系统进行整合统一, 以更方便的进行管理维护。
    >
    > 1. 将各个系统业务进行拆分以微服务化, 以`RESTful`方式提供服务
    > 2. 前后端分离, 前端项目进行为服务化
    > 3. 使用`Jenkins`+`Docker`进行持续部署

  - 技术工具:
    > Spring Cloud
    > Spring Boot
    > [Vue.js](https://cn.vuejs.org)
    > [Element](https://element.eleme.cn/#/zh-CN)
    > Redis
    > RabbitMQ
    > MySql

- **RIPED LIFE(社区管理系统 + 混合App)**
  - 项目时间: 2016.10-2017.10
  - 项目描述:
    > 第一期项目, 功能暂时只包括社区内的物业、动态、通知公告、报修、卫生所、幼儿天地、二手市场、房屋租赁、家政服务及其他附带功能。

  - 技术工具:
    > 后台框架: SpringMVC + Spring + Hibernate
    > 前端框架: [AdminLTE](https://adminlte.io)
    > App框架: [AppCan](https://www.appcan.cn)
    > 数据库: Oracle
    > 后台系统DEMO: http://www.admineap.com

- **研究院员工人事系统**
  - 项目时间: 2014.04-2016.10
  - 项目描述:
    > 对院内所内的关于人事的信息进行整合管理，比如员工信息、考勤、请假、考核、奖金、师带徒计划、专家库、培训管理、会议签到、流程及日常管理。

  - 技术工具
    > 后台框架: SpringMVC + Spring + Hibernate
    > 前端框架: [DHTMLX](https://docs.dhtmlx.com)
    > 数据库: Oracle
