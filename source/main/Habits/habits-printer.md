---
title: Printer
date: 2019-07-11 10:05:00
---

- 双面打印，一面一页
![Printer](./imgs/print-way-double-2.png)

- 双面打印，一面两页
![Printer](./imgs/print-way-double-4.png)
