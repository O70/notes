---
title: Developments
date: 2019-01-21 08:00:00
---

# Developments

## Kits

- [GitHub](https://github.com)
- [Gitee](https://gitee.com)
- [GitLab](http://10.27.213.70)
- Old: [Nexus](http://10.122.163.99:8081)
- [Nexus](http://10.122.163.216:8081)
- [MinDoc](http://10.27.213.99:8181/)[**123456**]
<!-- @!#PTRdocker@<key> -->
- [Docker Hub](http://10.122.163.216:5000)
- [Library](http://10.27.213.192:8888/BOOK/indexAdmin) <!-- Library-src.tar.gz 70 is database -->
- [Hadoop](http://10.27.213.65:50070)
- [Hadoop Release](http://10.27.213.66:50070)
- [Jenkins](http://10.122.163.216:8090/jenkins/) `test50:PTRkygl@413` <!-- kygl/@!#PTRkygl@201 -->

## Service Grid

<!-- PTR<key>413 -->
| Location | Eureka | Monitor | Zipkin | Admin-UI | User-UI |
| --- | --- | --- | --- | --- | --- |
| Local | http://10.122.163.77:8761 | http://10.122.163.77:8060 | http://10.122.163.77:9411 | None | None |
| Dev | http://10.27.213.167:8761 | http://10.27.213.167:8060 | http://10.27.213.167:9411 | http://10.27.213.167:8080 | http://10.27.213.167 |
| Test(59) | http://11.11.141.59:30000 | http://11.11.141.59:30003 | http://11.11.141.59:30004 | http://11.11.141.59:30101 | http://11.11.141.59:30102 |

**PTR[config]413**

## MySQL

| Props | Dev | Test | gray-Prod | Prod |
| :-----: | :---: | :----: | :----: | :----: |
| Host | 10.27.213.195 | 11.11.141.59 | 10.30.227.17 | 10.30.227.38 |
| Port | 8066 | 8066 | 3306 | 3306 |
| User | root | root | root | root |
| Password | MYSQLPTRkygl@[KEY] | MYSQLPTRkygl@[KEY] | CMPmysql@[KEY]esp | CMPmysql@[KEY]esp |

## System List

- [HRMIS](http://10.27.213.8:8080/)

## Developer Requirements

```

### 普通开发人员:

- 能熟练搭建需要的开发环境
- 熟悉数据库CRUD基本操作
- 熟悉Git, Maven等工具的基本操作
- 能自主学习某个新框架, 参考DEMO完成某个功能的基本实现
- 能理解并完成已分解好的子任务
- 能独立完成单独功能模块的开发

### 高级开发人员:

- 所用编程语言基础扎实, 理解IO、多线程等基础框架
- 能够进行库表设计, 对主流关系型/非关系型数据库能熟练使用
- 对微服务, 分布式, 搜索引擎有一定了解
- 熟悉Linux开发环境
- 良好的模块化设计，优秀的代码规范，熟悉行业最常见的框架
- 良好的沟通能力, 逻辑分析能力, 以及解决问题的能力
```
