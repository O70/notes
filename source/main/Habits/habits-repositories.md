---
title: Repositories
date: 2019-05-21 09:00:00
---

# Repositories

- Notes
```sh
$ git clone https://github.com/O70/Notes.git
$ git clone https://gitee.com/Guiwang/Notes.git
```

- config-hub(对应**Mac**中的**Documents**)
```sh
$ git clone -b notes https://github.com/O70/config-hub.git
```

- IDEA-Windows
```sh
$ git clone https://github.com/O70/IDEA-Windows.git
```

- IDEA-Mac
```sh
$ git clone https://github.com/O70/IDEA-Mac.git
```

- Shells
```sh
$ git clone https://gitee.com/Guiwang/Shells.git
```

- ST3(Sublime Text 3 Config)
```sh
$ git clone https://gitee.com/Guiwang/ST3.git
```

- credit-bill
```sh
$ https://git.dev.tencent.com/THRAEX/credit-bill.git
```
