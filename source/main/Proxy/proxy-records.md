---
title: Records
date: 2019-11-06 08:00:00
---

## Client

### Windows

- `ShadowsocksR-dotnet4.0.exe`

- `Clash for Windows`

### Android

- `SSR`

- `Surfboard`

- `Kitsunebi`

- `v2rayNG`

### IOS

- `Shadowrocket`

- `Quantumult`

Apple ID(US): [kaiprolad@gmail.com](URL:500ml.name)

### Mac

- [`ShawdowsockX-NG-R8`](https://github.com/VeniZ/ShadowsocksX-NG-R8-Bakup)/[`ShadowsocksX-NG-R`](https://github.com/qinyuhang/ShadowsocksX-NG-R)

- [`ClashX`](https://github.com/Dreamacro/clash)

### Linux

- `clash`

- [**`electron-ssr`**](https://github.com/qingshuisiyuan/electron-ssr-backup)

## Configuration Sample

### Clash

`$HOME/.config/clash`:

- `config.yaml`
``` yaml
#---------------------------------------------------#
## 配置文件需要放置在 $HOME/.config/clash/*.yaml

## 这份文件是clashX的基础配置文件，请尽量新建配置文件进行修改。
## ！！！只有这份文件的端口设置会随ClashX启动生效

## 如果您不知道如何操作，请参阅 SS-Rule-Snippet：https://github.com/Hackl0us/SS-Rule-Snippet/blob/master/LAZY_RULES/clash.yaml
## 或者官方Github文档 https://github.com/Dreamacro/clash/blob/master/README.md
#---------------------------------------------------#

port: 7890
socks-port: 7891
allow-lan: false
mode: Rule
log-level: info
external-controller: 127.0.0.1:9090

Proxy:

Proxy Group:

Rule:
- DOMAIN-SUFFIX,google.com,DIRECT
- DOMAIN-KEYWORD,google,DIRECT
- DOMAIN,google.com,DIRECT
- DOMAIN-SUFFIX,ad.com,REJECT
- GEOIP,CN,DIRECT
- MATCH,DIRECT
```

### ShawdowsockX-NG-R8

`$HOME/.ShawdowsockX-NG/*`

> https://500ml.pw
> [Subscribe](https://www.500ml.club/link/sJtu2tPPcHdA4k6P?shadowrocket=1)

### Su

https://500ml.info/link/sJtu2tPPcHdA4k6P?shadowrocket=1
https://raw.githubusercontent.com/Hackl0us/Surge-Rule-Snippets/master/LAZY_RULES/Shadowrocket.conf
