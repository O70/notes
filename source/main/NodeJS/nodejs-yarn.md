---
title: yarn
date: 2019-08-24 09:52:00
---

## Install

```sh
$ npm ls --depth=0 --global
$ npm search yarn
$ npm install -g yarn
$ yarn -v
```

## Usage

> https://yarn.bootcss.com/docs/usage/
