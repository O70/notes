---
title: NPM Settings
date: 2019-04-01 09:00:00
---

## Manage the npm configuration files

### Proxy

```
$ npm config list
```

```
$ npm config set proxy http://server:port
$ npm config set https-proxy http://server:port
$ npm config set registry https://registry.npm.taobao.org

$ npm config set registry https://registry.npmjs.org
```

```
$ npm config delete proxy
$ npm config delete https-proxy
```

## Packages

有两种方式用来安装npm包: **本地安装(`npm install` 默认行为)**和**全局安装**.

- **dependencies**: 生产环境中所必须的
- **devDependencies**: 开发与测试所使用的包

### 本地安装

| Command | Describe |
| ------- | -------- |
| `npm install` | 安装全部依赖 |
| `npm install <package_name>` | 本地安装 |
| `npm install <package_name> --save` | 添加**dependencies**依赖 |
| `npm install <package_name> --save-dev` or `npm install <package_name> -D` | 添加**devDependencies**依赖 |

### `npm install <package_name>`

- 会把`<package_name>`包安装到**node_modules**目录中
- 不会修改`package.json`
- 之后运行`npm install`命令时，不会自动安装`<package_name>`

- flags:
  - -S, --save: 包将出现在**dependencies**. 
  - -P, --save-prod: 包将出现在**dependencies**. 默认值,除非`-D`或`-O`出现.
  - -D, --save-dev: 包将出现在**devDependencies**.
  - -O, --save-optional: 包将出现在**optionalDependencies**.
  - –no-save: 防止保存到**dependencies**.
  - -E, --save-exact: 安装包，默认会安装最新的版本.
  - -g: 全局安装
