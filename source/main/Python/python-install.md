---
title: Install Python and pip
date: 2019-06-17 13:40:00
---

## On Windows10

### Install Python3

1. Download [Python 3.7.0](https://www.python.org/downloads/release/python-370/), choose **Windows x86-64 embeddable zip file**

2. 解压至`C:\Workspace\Kits\python3.7`

3. 配置环境变量`%KITS_HOME%\python3.7`

4. Open terminal: `python -V` or `python`

**Questions:**

- 输入`help`/`exit()`报以下错误
```
>>> help
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'help' is not defined

>>> exit()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'exit' is not defined
```

**打开`python37._pth`文件，去掉`# import site`的注释，然后保存。上述问题解决。**

**`python37._pth`**:
```
python37.zip
.

# 与import site作用相同
# Lib\site-packages

# Uncomment to run site.main() automatically
import site

```

> 开启`import site`很重要，否则接下来安装`pip`也会失败。

### Install pip

> [Do I need to install pip?](https://pip.pypa.io/en/stable/installing/)

1. Download and install
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

python get-pip.py
```

2. 配置环境变量`%KITS_HOME%\python3.7\Scripts`

3. New terminal
```
pip

Traceback (most recent call last):
  File "runpy.py", line 193, in _run_module_as_main
  File "runpy.py", line 85, in _run_code
  File "C:\Workspace\Kits\python3.7\Scripts\pip.exe\__main__.py", line 5, in <module>
ModuleNotFoundError: No module named 'pip'
```
**在`python37._pth`中去掉`# import site`的注释或添加`Lib\site-packages`**

### [查找模块](https://docs.python.org/zh-cn/3/using/windows.html?highlight=_pth#finding-modules)

**`python37._pth` to `python37._pth_bak`**

Python通常将其库（以及您的site-packages文件夹）存储在安装目录中。因此，如果您已将Python安装到`C:\Python\`，则默认库将驻留在 `C:\Python\Lib\`中，第三方模块存储在`C:\Python\Lib\site-packages\`。

若要完全覆盖`sys.path`，请创建与DLL(`python37._pth`)或可执行文件(`python._pth`)同名的`._pth`文件，并为要添加的每个路径指定一行`sys.path`。基于DLL名称的文件覆盖基于可执行文件的文件，如果需要，可以为加载运行时的任何程序限制路径。

当文件存在时，将忽略所有注册表和环境变量，启用隔离模式，并且：除非文件中的一行指定`import site`，否则不会导入`site`。以`＃`开头的空白路径和行将被忽略。每个路径可以是绝对的或相对于文件的位置。不允许使用除`site`以外的导入语句，并且不能指定任意代码。

请注意，当指定`import site`时，`.pth`文件（没有前导下划线）将由`site`模块正常处理。

当找不到`._pth`文件时，`sys.path`是如何在Windows上填充的：

- 在开始时，添加一个空条目，该条目对应于当前目录。

- 如果环境变量`PYTHONPATH`存在，如`环境变量`中所述，则接下来添加其条目。请注意，在Windows上，此变量中的路径必须用分号分隔，以区别于驱动器标识符中使用的冒号（`C:\`等）。

- 附加的`"application paths"`可以同时添加到注册表`HKEY_CURRENT_USER`和`HKEY_LOCAL_MACHINE`分支下的: `samp:\SOFTWARE\Python\PythonCore\{version}\PythonPath`中作为子键。以分号分隔的路径字符串作为默认值的子键将导致每个路径添加到`sys.path`。（请注意，所有已知的安装程序都只使用HKLM，因此HKCU通常为空。）

- 如果设置了环境变量`PYTHONHOME`，则将其假定为 “Python 主目录” 。否则，主Python可执行文件的路径用于定位 “landmark 文件” （`Lib\os.py`或`pythonXY.zip`）以推断 ”Python 主目录“ 。如果找到了Python主目录，则基于该文件夹将相关的子目录添加到`sys.path`（`Lib` , `plat-win`等）。否则，核心Python路径是从存储在注册表中的PythonPath构造的。

- 如果找不到Python Home，也没有指定`PYTHONPATH`环境变量，并且找不到注册表项，则使用具有相对条目的默认路径（例如`.\Lib;` `.\plat-win`等等）。

如果在主可执行文件旁边或在可执行文件上一级的目录中找到 pyvenv.cfg 文件，则以下变体适用：

- 如果``home`` 是一个绝对路径，并且`PYTHONHOME`未设置，则在推断起始位置时使用此路径而不是主可执行文件的路径

这一切的最终结果是：

- 运行`python.exe`，或主Python目录中的任何其他.exe（安装版本，或直接来自PCbuild目录）时，推导出核心路径，并忽略注册表中的核心路径。始终读取注册表中的其他“应用程序路径”。

- 当Python托管在另一个.exe（不同的目录，通过COM嵌入等）时，将不会推断出“Python Home”，因此使用了来自注册表的核心路径。始终读取注册表中的其他“应用程序路径”。

- 如果Python找不到它的主目录并且没有注册表值（冻结的.exe，一些非常奇怪的安装设置），那么你会得到一条带有一些默认但相对的路径的路径。

对于那些想要将Python捆绑到其应用程序或发行版中的人，以下建议将防止与其他安装冲突：

- 在您的可执行文件中包含一个`._pth`文件，其中包含目录。这将忽略注册表和环境变量中列出的路径，并忽略`site`，除非列出`import site`。

- 如果你在自己的可执行文件中加载`python3.dll`或`python37.dll`，在`Py_Initialize()`之前，要显式调用`Py_SetPath()`或（至少）`Py_SetProgramName()`

- 清除和/或 覆盖`PYTHONPATH`并在启动来自应用程序的`python.exe`之前设置`PYTHONHOME`。

- 如果您不能使用前面的建议（例如，您是一个允许人们直接运行：`file:python.exe`的分发版），请确保安装目录中存在`landmark`文件（:file:`Lib\os.py`）。（请注意，在zip文件中不会检测到该文件，但会检测到正确命名的zip文件。）

这些将确保系统范围安装中的文件不会优先于与应用程序捆绑在一起的标准库的副本。否则，用户可能会在使用您的应用程序时遇到问题请注意，第一个建议是最好的，因为其他建议可能仍然容易受到注册表和用户站点包中的非标准路径的影响。

在 3.6 版更改:

- 添加`._pth`文件支持并从`pyvenv.cfg`中删除`applocal`选项

- 当直接与可执行文件相邻时，添加`pythonXX.zip`作为潜在的`landmark`。

3.6 版后已移除:
在`Modules`（不是`PythonPath`）下的注册表中指定的模块可以通过以下方式导入`importlib.machinery.WindowsRegistryFinder`。在Windows上，此查找程序在3.6.0及更早版本的可用，但可能需要在将来显式添加到`sys.meta_path`
