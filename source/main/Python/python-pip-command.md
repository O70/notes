---
title: pip command
date: 2019-06-10 16:10:00
---

## Command

```sh
$ pip --version

$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py   #! 下载安装脚本
$ sudo python get-pip.py    #! 运行安装脚本

$ pip --version
$ pip --help

$ pip install -U pip
# or
$ sudo easy_install --upgrade pip
```

- Install package
```sh
$ pip install SomePackage              #! 最新版本
$ pip install SomePackage==1.0.4       #! 指定版本
$ pip install 'SomePackage>=1.0.4'     #! 最小版本

$ pip install Django==1.7

$  pip install --proxy http://proxy_server:8080 requests #! 使用代理
```

- Upgrade package
```sh
$ pip install --upgrade SomePackage
```

升级指定的包，通过使用==, >=, <=, >, < 来指定一个版本号。

- Uninstall package
```sh
$ pip uninstall SomePackage
```

- Search package
```sh
$ pip search SomePackage
```

- Show pakcage info
```sh
$ pip show
$ pip show requests
$ pip show -f requests
```

- List package
```sh
$ pip list
$ pip list -o #! 查看可升级的包
```

- `requirements.txt`
```sh
$ pip freeze > requirements.txt

$ pip install -r requirements.txt

$ pip install --proxy http://proxy_server:8080 -r requirements.txt

# python3
$ pip install pipreqs
$ pipreqs ./
```
