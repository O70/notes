---
title: pip settings
date: 2019-11-01 11:10:00
---

pip version: `pip 19.3.1`

## Global Config

`~/.config/pip/pip.conf`:

```
[global]
proxy=http://proxy1.bj.petrochina:8080/
index-url=http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com
```

- Install
``` sh
$ pip install --proxy http://proxy1.bj.petrochina:8080 -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com GitPython
```
