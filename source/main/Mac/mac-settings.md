---
title: Mac Settings
date: 2019-06-21 08:25:00
---

> Modification Mouse Speed
```sh
$ defaults read -g com.apple.mouse.scaling

#!! Max:10
$ defaults write -g com.apple.mouse.scaling 7
```

> .DS_store
* 禁止: `defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool TRUE`
* 恢复: `defaults delete com.apple.desktopservices DSDontWriteNetworkStores`

> 查看端口号
```sh
$ sudo lsof -i tcp:[PORT]
$ kill [PID]
```

> Uninstall Foxmail
```sh
$ ls ~/Library/Containers/com.tencent.Foxmail/
$ ls ~/Library/Application\ Scripts/com.tencent.Foxmail/
```

## Mac Plugin

- Polyglot

## Shell

`bash` to `zsh`: https://support.apple.com/en-us/HT208050

``` sh
chsh -s /bin/zsh

chsh -s /bin/bash
```

- `.zprofile`等同于`.bash_profile`
- `.zshrc`等同于`.bashrc`

`git completion`: https://github.com/git/git/tree/master/contrib/completion

### bash

``` sh
$ brew search bash-completion
$ brew install bash-completion
$ brew info bash-completion
```

``` sh
$ ls .bash_completion.d
git-completion.bash	gradle-completion.bash
```

__.bash_profile__
``` sh
[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

# add .git-completion.bash
# source ~/.git-completion.bash
source ~/.bash_completion.d/*.bash

export UTILITY_DIR=~/Workspace/Utility
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home
export CLASSPATH=.:JAVA_HOME/lib/dt.jar:JAVA_HOME/lib/tools.jar
export MAVEN_HOME=$UTILITY_DIR/maven

# weblogic-maven-plugin
#export ORACLE_HOME=$UTILITY_DIR/weblogic/Oracle/Middleware/Oracle_Home
#export MAVEN_HOME=$ORACLE_HOME/oracle_common/modules/org.apache.maven_3.2.5
#export MAVEN_OPTS="-DUseSunHttpHandler=true"

export M2_HOME=$MAVEN_HOME
export SPRING_HOME=$UTILITY_DIR/spring-cli
export PATH=$PATH:$MAVEN_HOME/bin:$SPRING_HOME/bin:$UTILITY_DIR/sonar-scanner/bin
export PATH=$PATH:/usr/local/Cellar/python/3.7.0/Frameworks/Python.framework/Versions/3.7/bin:$UTILITY_DIR/nodes/node-v10.16.3-darwin-x64/bin:$UTILITY_DIR/apache-ant-1.9.14/bin:$UTILITY_DIR/gradles/gradle-5.6.2/bin

alias c='clear'
alias r='reset'
```

### zsh

https://git-scm.com/book/zh/v2/附录-A:-其它环境中的-Git-Zsh-中的-Git

``` sh
# Create the folder structure
mkdir -p ~/.zsh
cd ~/.zsh

# Download the scripts
curl -o git-completion.bash https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
curl -o _git https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh
```

> 科学上网: https://medium.com/@oliverspryn/adding-git-completion-to-zsh-60f3b0e7ffbc

__.zshrc__
``` sh
# Load Git completion
zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash
fpath=(~/.zsh $fpath)

autoload -Uz compinit && compinit

export UTILITY_DIR=~/Workspace/Utility
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home
export CLASSPATH=.:JAVA_HOME/lib/dt.jar:JAVA_HOME/lib/tools.jar
export MAVEN_HOME=$UTILITY_DIR/maven

export M2_HOME=$MAVEN_HOME
export SPRING_HOME=$UTILITY_DIR/spring-cli
export PATH=$PATH:$MAVEN_HOME/bin:$SPRING_HOME/bin:$UTILITY_DIR/sonar-scanner/bin
export PATH=$PATH:/usr/local/Cellar/python/3.7.0/Frameworks/Python.framework/Versions/3.7/bin:$UTILITY_DIR/nodes/node-v10.16.3-darwin-x64/bin:$UTILITY_DIR/apache-ant-1.9.14/bin:$UTILITY_DIR/gradles/gradle-5.6.2/bin

alias c='clear'
alias r='reset'
```
