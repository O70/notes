---
title: JetBrains ToolBox App
date: 2019-12-11 11:25:00
---

## Open Toolbox App

> https://apple.stackexchange.com/questions/366542/install-spotify-cant-be-opened-because-apple-cannot-check-it-for-malicious-so
> "JetBrains Toolbox" can't be opened because Apple cannot check it for malicious software

- **for older version of macOS**

`System Preferences -> Security & Privacy -> check Allow Anywhere`

- **for macOS Sierra, High Sierra, Mojave**

``` sh
$ sudo spctl --master-disable
```

But if the Move To Trash issue still exists. This is because Apple has removed TNT certification. You may use codesign to resign it:

``` sh
$ codesign --sign - --force --deep <app-path>
```

- **for macOS Catalina**

I think `codesign` sometimes doesn't work for the **notarization** issue recently, so you should use xattr to remove the quarantine:

``` sh
$ xattr -d com.apple.quarantine <app-path>
```

I have got a conclusion. When installing app from 3rd party on different macOS, you can try different ways when it comes up to `Move to Trash issue`. Meanwhile, I think this is a common issue for 3rd party apps, so you may change the title question to something that contains `Cannot be opened` or `Move to trash` as well.

## Modify

- Preferences
`~/Library/Preferences`

- Toolbox
``` sh
$ xattr -d com.apple.quarantine /Applications/JetBrains\ Toolbox.app
```

- Toolbox Item
``` sh
$ xattr -d com.apple.quarantine ~/Applications/JetBrains\ Toolbox/*.app
# or
$ xattr -d com.apple.quarantine ~/Applications/JetBrains\ Toolbox/CLion.app
$ xattr -d com.apple.quarantine ~/Applications/JetBrains\ Toolbox/DataGrip.app
$ xattr -d com.apple.quarantine ~/Applications/JetBrains\ Toolbox/IntelliJ IDEA Ultimate.app
$ xattr -d com.apple.quarantine ~/Applications/JetBrains\ Toolbox/PyCharm Community.app
$ xattr -d com.apple.quarantine ~/Applications/JetBrains\ Toolbox/WebStorm.app
```
