---
title: Three.js
date: 2018-10-08 09:00:00
---

# Three.js

## Chapter 1

渲染器：

- 基于WebGL
- 基于CSS-3D
- 基于画板
- 基于SVG

画板和SVG十分消耗CPU资源，而且缺乏对一些功能的支持，比如材质和阴影。

- Scene
- Camera
- Renderer
- 材质
- 光源
- 阴影

启动简单Web服务器：
```py
python -m SimpleHTTPServer 8080
```

## Chapter 2

组件:

- 摄像机，决定屏幕上哪些东西需要渲染
- 光源，决定材质如何显示以及用于产生阴影
- 对象，摄像机透视图里主要的渲染对象，e.g.: 方块，球体

THREE.Scene method:

- Add()
- Remove()
- children
- getObjectByName()
- traverse()

THREE.Scene attribute:

- fog(雾化)
- overrideMaterial(材质覆盖)

- 正交投影摄像机
- 透视投影摄像机

## Chapter 3
