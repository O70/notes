---
title: Deploying the Development Environment
date: 2019-04-01 09:00:00
---

## Start the Docker Container

[Run Image](../Docker/docker-run-image.html)

```sh
$ docker start c-mysql; docker start c-zipkin
```

## Services

- Registry
```sh
$ mvn spring-boot:run \
  -DSERVER_IP=10.122.163.77 \
  -DEUREKA_URL=http://eureka:eureka@10.122.163.77:8761/eureka \
  -DMONITOR_URL=http://10.122.163.77:8060 \
  -DZIPKIN_URL=http://10.122.163.77:9411
```

- Monitor
```sh
$ mvn spring-boot:run \
  -DSERVER_IP=10.122.163.77 \
  -DEUREKA_URL=http://eureka:eureka@10.122.163.77:8761/eureka
```

- Config
```sh
...
```

- Gateway
```sh
$ mvn spring-boot:run \
  -DSERVER_IP=10.122.163.77 \
  -DEUREKA_URL=http://eureka:eureka@10.122.163.77:8761/eureka \
  -DZIPKIN_URL=http://10.122.163.77:9411
```

- Admin
```sh
$ mvn spring-boot:run \
  -DSERVER_IP=10.122.163.77 \
  -Dcnpc.eureka.service.url=http://eureka:eureka@10.122.163.77:8761/eureka \
  -Dcnpc.zipkin.url=http://10.122.163.77:9411
```

- Food
```sh
$ mvn spring-boot:run \
  -DSERVER_IP=10.122.163.76 \
  -Dcnpc.eureka.service.url=http://eureka:eureka@10.122.163.77:8761/eureka \
  -Dcnpc.zipkin.url=http://10.122.163.77:9411 \
  -Dcnpc.datasource.url=jdbc:mysql://10.122.163.77:3306/esp_food \
  -Dcnpc.datasource.username=hanzo \
  -Dcnpc.datasource.password=hanzo70
```
