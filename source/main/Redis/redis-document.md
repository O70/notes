---
title: Redis Document
date: 2019-07-03 18:20:00
---

## Redis for Docker

- Run Image
```sh
$ docker run -d \
    --name redis-server \
    -p 6379:6379 \
    --restart always \
    -v $PWD/Dockers/redis/data:/data:Z \
    -v $PWD/Dockers/redis/redis.conf:/usr/local/etc/redis/redis.conf:Z \
    redis:latest \
    redis-server /usr/local/etc/redis/redis.conf
```

- Connect Remote
```sh
$ docker run -it \
    --name redis-cli \
    --rm \
    redis:latest \
    redis-cli -h 10.27.213.67 -p 6379 -a PWD
```

## Command

| Command | Describe |
| ------- | -------- |
| `KEYS *` | 列出所有`key` |
| `SET key value` | 设置指定`key`的值 |
| `GET key` | 获取指定`key`的值 |
| `DEL key` | 用于在`key` 存在时删除`key` |
| `DUMP key` | 序列化给定`key` ，并返回被序列化的值 |
| `EXISTS key` | 检查给定`key` 是否存在 |
| `EXPIRE key seconds` | 为给定`key` 设置过期时间，以秒计 |
| `EXPIREAT key timestamp` | `EXPIREAT`的作用和`EXPIRE`类似，都用于为`key`设置过期时间。不同在于`EXPIREAT`命令接受的时间参数是`UNIX`时间戳(`unix timestamp`) |
| `PEXPIRE key milliseconds` | 设置`key`的过期时间以毫秒计 |
| `PEXPIREAT key milliseconds-timestamp` | 设置`key`过期时间的时间戳(`unix timestamp`)以毫秒计 |
| `KEYS pattern` | 查找所有符合给定模式(`pattern`)的`key` |
|	`MOVE key db` | 将当前数据库的`key`移动到给定的数据库`db`当中 |
| `PERSIST key` | 移除`key`的过期时间，`key`将持久保持 |
|	`PTTL key` | 以毫秒为单位返回`key`的剩余的过期时间 |
|	`TTL key` | 以秒为单位，返回给定`key`的剩余生存时间(`TTL, time to live`) |
|	`RANDOMKEY` | 从当前数据库中随机返回一个`key` |
|	`RENAME key newkey` | 修改`key`的名称 |
|	`RENAMENX key newkey` | 仅当`newkey`不存在时，将`key`改名为`newkey` |
|	`TYPE key` | 返回`key`所储存的值的类型 |
