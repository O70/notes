---
title: Concepts
date: 2019-08-21 10:02:00
categories:
- Technology
- Webpack
tags:
- webpack
- concepts
---

## 入口起点(entry points)

**入口起点(entry point)** 指示`webpack`应该使用哪个模块，来作为构建其内部 [依赖图(dependency graph)](https://webpack.docschina.org/concepts/dependency-graph/) 的开始。进入入口起点后，`webpack`会找出有哪些模块和库是入口起点（直接和间接）依赖的。

**默认值是`./src/index.js`**

- 单个入口（简写）语法: `entry: string|Array<string>`
- 对象语法: `entry: {[entryChunkName: string]: string|Array<string>}`

## 输出(output)

配置`output`选项可以控制`webpack`如何向硬盘写入编译文件。注意，即使可以存在多个`entry`起点，但只指定一个`output`配置。

`output`属性的最低要求是，将它的值设置为一个对象，包括以下属性：
- `filename`用于输出文件的文件名。

## 模式(mode)

> 可能的值有：none, development 或 production（默认）。

| Option | Description |
| ------ | ----------- |
| `development` | 会将`DefinePlugin`中`process.env.NODE_ENV`的值设置为`development`。启用 `NamedChunksPlugin`和`NamedModulesPlugin`。 |
| `production` | 会将`DefinePlugin`中`process.env.NODE_ENV`的值设置为`production`。启用 `FlagDependencyUsagePlugin`, `FlagIncludedChunksPlugin`, `ModuleConcatenationPlugin`, `NoEmitOnErrorsPlugin`, `OccurrenceOrderPlugin`, `SideEffectsFlagPlugin`和`TerserPlugin`。 |
| `none` | 退出任何默认优化选项 |

## loader

有三种使用`loader`的方式：
- 配置(configuration)（推荐）：在 webpack.config.js 文件中指定 loader。
- 内联(inline)：在每个 import 语句中显式指定 loader。
- CLI：在 shell 命令中指定它们。

[特性](https://webpack.docschina.org/concepts/loaders/#loader-特性)

## 插件(plugin)

插件是`webpack`的 **支柱** 功能。

**插件目的在于解决`loader`无法实现的其他事。**

## 配置(configuration)

**webpack 配置是标准的 Node.js CommonJS 模块**

## 模块(module)

**webpack将 __模块__ 的概念应用于项目中的任何文件。**

## 为什么选择`webpack`

在打包工具出现之前，在浏览器中运行`JavaScript`有两种方法:
- 引用一些脚本来存放每个功能；此解决方案很难扩展，因为加载太多脚本会导致网络瓶颈。
- 使用一个包含所有项目代码的大型 .js 文件，但是这会导致作用域、文件大小、可读性和可维护性方面的问题。

[立即调用函数表达式 - Immediately invoked function expressions](https://webpack.docschina.org/concepts/why-webpack/#立即调用函数表达式-immediately-invoked-function-expressions)

**Node.js 是一个 JavaScript 运行时，可以在浏览器环境之外的计算机和服务器中使用。webpack 运行在 Node.js 中。**

## 模块解析(module resolution)

**resolver 是一个库(library)，用于帮助找到模块的绝对路径。**

## 依赖图(dependency graph)

## manifest

## 部署目标(target)

## 模块热替换(hot module replacement)
