---
title: Configuration
date: 2019-08-21 16:36:00
categories:
- Technology
- Webpack
tags:
- webpack
- guides
---

## Getting Started

> https://webpack.docschina.org/guides/getting-started/

- Init
```
mkdir webpack-demo && cd webpack-demo
git init
npm init -y
npm install webpack webpack-cli --save-dev
```

- Install Dependency
`npm install --save lodash`

`npm install style-loader css-loader --save-dev`

`npm install --save-dev file-loader`

`npm install --save-dev csv-loader xml-loader`

`npm install --save-dev html-webpack-plugin`

`npm install --save-dev clean-webpack-plugin`

`npm install --save-dev webpack-dev-server`

`npm install --save-dev express webpack-dev-middleware`

`npm install --save-dev webpack-merge`

- Build
`npx webpack`

`npx webpack --config webpack.config.js`

`npm run build`
`npm run build -- --colors`

- Run
```
cd dist

npx serve
##! or
npx http-server
```

### loader

- `style-loader`
- `css-loader`
- `file-loader`
- `html-loader`
- `image-webpack-loader`
- `url-loader`
- `csv-loader`
- `xml-loader`

### plugin

- `html-webpack-plugin`: 默认生成它自己的`index.html`文件
- `html-webpack-template`: 除了提供默认模板之外，还提供了一些额外的功能
- `clean-webpack-plugin`: 一个流行的清理插件
- `terser-webpack-plugin`
- `babel-minify-webpack-plugin`
- `webpack-closure-compiler`
- `SplitChunksPlugin`: 用于将模块分离到单独的`bundle`中
- `HashedModuleIdsPlugin`: 修复`vendor hash`变化

- `mini-css-extract-plugin`：用于将`CSS`从主应用程序中分离。
- `bundle-loader`：用于分离代码和延迟加载生成的`bundle`。
- `promise-loader`：类似于`bundle-loader`，但是使用了`promise API`。

### webpack-dev-server

`webpack-dev-server`为你提供了一个简单的`web server`，并且具有`live reloading(实时重新加载)`功能。

> `webpack-dev-server`在编译之后不会写入到任何输出文件。而是将`bundle`文件保留在内存中，然后将它们`serve`到 `server`中，就好像它们是挂载在`server`根路径上的真实文件一样。如果你的页面希望在其他不同路径中找到`bundle`文件，则可以通过`dev server`配置中的`publicPath`选项进行修改。

## Hot Module Replacement

## Production

`development`(开发环境) 和 `production`(生产环境) 这两个环境下的构建目标存在着巨大差异。

- `development`: 在开发环境中，我们需要：强大的 source map 和一个有着 live reloading(实时重新加载) 或 hot module replacement(热模块替换) 能力的 localhost server。
- `production`: 生产环境目标则转移至其他方面，关注点在于压缩 bundle、更轻量的 source map、资源优化等，通过这些优化方式改善加载时间。由于要遵循逻辑分离，我们通常建议为每个环境编写彼此独立的`webpack`配置。

## Code Splitting

- 入口起点：使用`entry`配置手动地分离代码。手动配置较多，并有一些隐患。
- 防止重复：使用`SplitChunksPlugin`去重和分离`chunk`。
- 动态导入：通过模块中的内联函数调用来分离代码。

[bundle 分析(bundle analysis)](https://webpack.docschina.org/guides/code-splitting/#bundle-分析-bundle-analysis-)

## Lazy Loading

## Caching

## Authoring a Library

`library`的调用规范如下:

- ES2015 module import:
```js
import * as webpackNumbers from 'webpack-numbers';
// ...
webpackNumbers.wordToNum('Two');
```

- CommonJS module require:
```js
var webpackNumbers = require('webpack-numbers');
// ...
webpackNumbers.wordToNum('Two');
```

- AMD module require:
```js
require(['webpackNumbers'], function ( webpackNumbers) {
  // ...
  webpackNumbers.wordToNum('Two');
});
```

## Shimming

使用场景:
- 一些`third party(第三方库)`可能会引用一些全局依赖（例如`jQuery`中的`$`）。因此这些`library`也可能会创建一些需要导出的全局变量。这些 "broken modules(不符合规范的模块)" 就是`shim(预置依赖)`发挥作用的地方
- 当你希望`polyfill`扩展浏览器能力，来支持到更多用户时。在这种情况下，你可能只是想要将这些`polyfills`提供给需要修补(patch)的浏览器（也就是实现按需加载）

**shim 是一个库(library)，它将一个新的 API 引入到一个旧的环境中，而且仅靠旧的环境中已有的手段实现。polyfill 就是一个用在浏览器 API 上的 shim。我们通常的做法是先检查当前浏览器是否支持某个 API，如果不支持的话就按需加载对应的 polyfill。然后新旧浏览器就都可以使用这个 API 了。**

## Progressive Web Application
