---
title: Docker Run Image
date: 2018-10-21 09:00:00
---

# Docker Run Image

## MySQL

查看MySQL版本：
```sh
$ docker inspect mysql
...
"Env": [
    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "GOSU_VERSION=1.7",
    "MYSQL_MAJOR=5.7",
    "MYSQL_VERSION=5.7.21-1debian9"
],
...
```

運行鏡像：
```sh
$ docker run -d \
  --name c-mysql \
  -p 3306:3306 \
  -v $PWD/Dockers/mysql/conf:/etc/mysql/conf.d \
  -v $PWD/Dockers/mysql/data:/var/lib/mysql \
  -v $PWD/Dockers/mysql/logs:/log \
  -e MYSQL_ROOT_PASSWORD=hanzo \
  mysql:latest
```

``` sh
$ docker run -d \
  --name c-mysql-s \
  -p 3307:3306 \
  -v ~/Workspace/Dockers/mysql/conf/mysql.cnf:/etc/mysql/conf.d/mysql.cnf \
  -v ~/Workspace/Dockers/mysql/datas:/var/lib/mysql \
  -v ~/Workspace/Dockers/mysql/logs:/log \
  -e MYSQL_ROOT_PASSWORD=mysql \
  mysql:latest
```

解决中文乱码`mysql.cnf`:
```
[mysqld]
character-set-server=utf8
[client]
default-character-set=utf8
[mysql]
default-character-set=utf8
```

進入容器:

```sh
$ docker exec -it c-mysql bash
```
```sql
# mysql -u root -p
select version();
show databases
```

MySQL查看用戶信息：
```sql
desc mysql.user;
select Host, User from mysql.user;
show grants for 'root'@'%';
```

[創建用戶](https://www.cnblogs.com/zeroone/articles/2298942.html)：
```sh
#!! CREATE USER 'username'@'host' IDENTIFIED BY 'password';
CREATE USER 'hanzo'@'%' IDENTIFIED BY 'hanzo70';

#!! GRANT privileges ON databasename.tablename TO 'username'@'host'
GRANT ALL ON *.* TO 'hanzo'@'%';

#!! SET PASSWORD FOR 'username'@'host' = PASSWORD('newpassword');
#!! 如果是当前登陆用户用SET PASSWORD = PASSWORD("newpassword");

#!! REVOKE privilege ON databasename.tablename FROM 'username'@'host';

#!! DROP USER 'username'@'host';
```

e.g.:
```
# update container env for MYSQL_ROOT_PASSWORD
export MYSQL_ROOT_PASSWORD="<PREFIX>-hanzo"

# update root password
mysql -u root -p
SET PASSWORD = PASSWORD("<PREFIX>-hanzo");

# update hanzo password
set password for 'hanzo'@'%' = password("<PREFIX>-hanzo70");
```

```
SHOW CREATE TABLE TABLE_NAME;
ALTER DATABASE DB_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE TABLE_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
```

随机抽取一条数据:
```sql
SELECT * FROM tbl_goods
WHERE id >= (SELECT FLOOR(RAND() * (SELECT MAX(id) FROM tbl_goods)))
ORDER BY id LIMIT 1;

update tbl_goods_snapshot s set s.gid = (
    SELECT id FROM tbl_goods ORDER BY RAND() LIMIT 1
);
```

**mysqldump**：
 ```sh
 $ docker run --rm -it mysql mysqldump -h 10.27.213.67 -uroot -phanzo h-admin > h-admin.sql

 $ docker exec -it mysql mysqldump -uroot -phanzo h-admin > h_admin.sql
 ```

 ```
docker cp riped-admin.sql container_name
create database riped-admin
use riped-admin
source riped-admin.sql
 ```

## Zipkin

Create Database and Table:
```sql
DROP DATABASE IF EXISTS zipkin;
CREATE DATABASE zipkin;

CREATE TABLE IF NOT EXISTS zipkin_spans (
  `trace_id_high` BIGINT NOT NULL DEFAULT 0 COMMENT 'If non zero, this means the trace uses 128 bit traceIds instead of 64 bit',
  `trace_id` BIGINT NOT NULL,
  `id` BIGINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `parent_id` BIGINT,
  `debug` BIT(1),
  `start_ts` BIGINT COMMENT 'Span.timestamp(): epoch micros used for endTs query and to implement TTL',
  `duration` BIGINT COMMENT 'Span.duration(): micros used for minDuration and maxDuration query',
  PRIMARY KEY (`trace_id_high`, `trace_id`, `id`)
) ENGINE=InnoDB ROW_FORMAT=COMPRESSED CHARACTER SET=utf8 COLLATE utf8_general_ci;
ALTER TABLE zipkin_spans ADD INDEX(`trace_id_high`, `trace_id`) COMMENT 'for getTracesByIds';
ALTER TABLE zipkin_spans ADD INDEX(`name`) COMMENT 'for getTraces and getSpanNames';
ALTER TABLE zipkin_spans ADD INDEX(`start_ts`) COMMENT 'for getTraces ordering and range';
CREATE TABLE IF NOT EXISTS zipkin_annotations (
  `trace_id_high` BIGINT NOT NULL DEFAULT 0 COMMENT 'If non zero, this means the trace uses 128 bit traceIds instead of 64 bit',
  `trace_id` BIGINT NOT NULL COMMENT 'coincides with zipkin_spans.trace_id',
  `span_id` BIGINT NOT NULL COMMENT 'coincides with zipkin_spans.id',
  `a_key` VARCHAR(255) NOT NULL COMMENT 'BinaryAnnotation.key or Annotation.value if type == -1',
  `a_value` BLOB COMMENT 'BinaryAnnotation.value(), which must be smaller than 64KB',
  `a_type` INT NOT NULL COMMENT 'BinaryAnnotation.type() or -1 if Annotation',
  `a_timestamp` BIGINT COMMENT 'Used to implement TTL; Annotation.timestamp or zipkin_spans.timestamp',
  `endpoint_ipv4` INT COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_ipv6` BINARY(16) COMMENT 'Null when Binary/Annotation.endpoint is null, or no IPv6 address',
  `endpoint_port` SMALLINT COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_service_name` VARCHAR(255) COMMENT 'Null when Binary/Annotation.endpoint is null'
) ENGINE=InnoDB ROW_FORMAT=COMPRESSED CHARACTER SET=utf8 COLLATE utf8_general_ci;
ALTER TABLE zipkin_annotations ADD UNIQUE KEY(`trace_id_high`, `trace_id`, `span_id`, `a_key`, `a_timestamp`) COMMENT 'Ignore insert on duplicate';
ALTER TABLE zipkin_annotations ADD INDEX(`trace_id_high`, `trace_id`, `span_id`) COMMENT 'for joining with zipkin_spans';
ALTER TABLE zipkin_annotations ADD INDEX(`trace_id_high`, `trace_id`) COMMENT 'for getTraces/ByIds';
ALTER TABLE zipkin_annotations ADD INDEX(`endpoint_service_name`) COMMENT 'for getTraces and getServiceNames';
ALTER TABLE zipkin_annotations ADD INDEX(`a_type`) COMMENT 'for getTraces and autocomplete values';
ALTER TABLE zipkin_annotations ADD INDEX(`a_key`) COMMENT 'for getTraces and autocomplete values';
ALTER TABLE zipkin_annotations ADD INDEX(`trace_id`, `span_id`, `a_key`) COMMENT 'for dependencies job';
CREATE TABLE IF NOT EXISTS zipkin_dependencies (
  `day` DATE NOT NULL,
  `parent` VARCHAR(255) NOT NULL,
  `child` VARCHAR(255) NOT NULL,
  `call_count` BIGINT,
  `error_count` BIGINT,
  PRIMARY KEY (`day`, `parent`, `child`)
) ENGINE=InnoDB ROW_FORMAT=COMPRESSED CHARACTER SET=utf8 COLLATE utf8_general_ci;
```

Run:
```sh
$ docker run -d \
  --name c-zipkin \
  -p 9411:9411 \
  -e STORAGE_TYPE=mysql \
  -e MYSQL_HOST=10.122.163.77 \
  -e MYSQL_TCP_PORT=3306 \
  -e MYSQL_DB=zipkin \
  -e MYSQL_USER=hanzo \
  -e MYSQL_PASS=hanzo70 \
  openzipkin/zipkin:latest
```
