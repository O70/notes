---
title: Vue Dockerfile
date: 2019-07-15 14:33:00
---

**Project: `vue-laboratory`**

## nginx.template

```
# user  root;
worker_processes  1;

error_log  logs/error.log;
error_log  logs/error.log  notice;
error_log  logs/error.log  info;

pid        logs/nginx.pid;

events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;
    server {
        listen       80;
        server_name  localhost;

        location / {
            root   /usr/share/nginx/ui;
            try_files $uri $uri/ /index.html;
        }

      location /api {
         proxy_pass http://$ESP_GATEWAY_HOST/api;
       }

    }
}
```

## Dockerfile

```
FROM nginx:latest
MAINTAINER 鬼王

WORKDIR /usr/share/nginx/ui
WORKDIR /etc/nginx/logs

COPY ./dist/ /usr/share/nginx/ui/

RUN chmod -R 777 /usr/share/nginx/ui/

RUN rm -rf /etc/nginx/conf.d/*

COPY nginx.template /etc/nginx/nginx.template

ENV ESP_GATEWAY_HOST 10.10.0.154:8765

CMD envsubst '$ESP_GATEWAY_HOST' < /etc/nginx/nginx.template > /etc/nginx/nginx.conf && nginx -g 'daemon off;'

EXPOSE 80
```

## Commands

```sh
#!! $ npm install --registry=https://registry.npm.taobao.org

 export ESP_GATEWAY_HOST=https://www.baidus.com
$ npm run build
$ export -n ESP_GATEWAY_HOST

#!! or
#!! "build:esp": "ESP_GATEWAY_HOST=https://www.baidus.com node build/build.js ; export -n ESP_GATEWAY_HOST"

$ docker build -t guiwang/test-ui .

$ docker run -d --name test-ui -p 8081:80 guiwang/test-ui

#!! 以下方式环境变量无效
$ docker run -d --name test-ui2 -p 8082:80 -e ESP_GATEWAY_HOST=www.baidu.com guiwang/test-ui

$ docker rm -v $(docker stop test-ui)
$ docker rmi guiwang/test-ui:latest
```
