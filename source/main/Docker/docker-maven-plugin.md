---
title: docker-maven-plugin
date: 2019-05-31 18:00:00
---

## Dockerfile

```
FROM 10.122.163.99:5000/java:8u171
VOLUME /tmp
ADD target/*.jar app.jar
RUN sh -c 'touch /app.jar' \
	&& echo $(date) > /image_built_at \
	&& /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

## pom.xml

```xml
<plugins>
  <plugin>
      <groupId>com.spotify</groupId>
      <artifactId>docker-maven-plugin</artifactId>
      <version>1.0.0</version>
      <configuration>
          <imageName>${docker.image.prefix}/${project.artifactId}</imageName>
          <dockerDirectory>${project.basedir}</dockerDirectory>
          <resources>
              <resource>
                  <targetPath>/</targetPath>
                  <directory>${project.build.directory}</directory>
                  <include>${project.build.finalName}.jar</include>
              </resource>
          </resources>
      </configuration>
  </plugin>
</plugins>
```

## Command Usage

- You can build an image with the above configurations by running this command.
```
mvn clean package -Dmaven.test.skip=true docker:build
```

- To push the image you just buit to the registry, specify the `pushImageTag` flag.
```
mvn clean package -Dmaven.test.skip=true docker:build -DpushImageTag
```

## Local Deploy

因网络问题，未用到`docker-maven-plugin`。

在本地执行以下命令：
```
cd ../<PROJECT_DIR>
docker login -u docker --password <pwd> 10.122.163.216:5000
docker build -t 10.122.163.216:5000/prod-esp-sms:1.0 .
docker push 10.122.163.216:5000/prod-esp-sms:1.0
```

进入服务部署应用：
```
ssh root@10.27.213.85
docker login -u docker --password <pwd> 10.122.163.216:5000
docker pull 10.122.163.216:5000/prod-esp-sms:1.0
docker run -d --name prod-esp-sms -p 8087:8087 10.122.163.216:5000/prod-esp-sms:1.0
```
