---
title: Docker Hub for guiwang/ubuntu-dev
date: 2019-06-2 12:00:00
---

## Run Image

- Windows 10

```
docker run -it --name ubuntu-dev -p 9090:80 -p 9091:8080 -p 9092:8081 -p 9093:8082 -v /c/Workspace:/Workspace guiwang/ubuntu-dev
```

**Notes**:
- 如果要正常退出不关闭容器，请按Ctrl+P+Q进行退出容器
- 如果使用exit退出，那么在退出之后会关闭容器，可以使用下面的流程进行恢复
	- 使用docker restart命令重启容器
	- 使用docker attach/exec命令进入容器

## Into the Container

`docker exec -it ubuntu-dev bash`

## Remove Container

`docker rm -v ubuntu-dev`
