---
title: Update Kernel
date: 2019-06-22 11:30:00
---

## CentOS 6/7

1. `yum clean all && yum makecache`，进行软件源更新；
2. `yum update kernel -y`，更新当前内核版本;
3. `reboot`，更新后重启系统生效;
4. `uname -a`，检查当前版本是否为上述【安全版本】，如果是，则说明修复成功。
