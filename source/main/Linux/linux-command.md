---
title: Linux Command
date: 2019-06-08 14:00:00
---

## `tar`

- Create archive
```sh
$ tar -cf <file>.tar <file>...

$ tar -czf <file>.tar.gz <file>...
```

- View detail for archive
```sh
$ tar -tvf file.tar.gz
```

- Extract
```sh
$ tar -xf <file>.tar
```
