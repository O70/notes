---
title: RabbitMQ in Action
date: 2018-04-06 09:00:00
---

## Chapter 2

**Advanced Message Queuing Protocol**

- Exchange
- Queue
- Bind

Method and Parameter:

- basic.consume()
- basic.get()
- basic.ack()
- auto_ack
- basic.reject()
- queue.declare([passive])
- exchange.declare()
- durable, default: false, 服务器重启后是否重建Queue/Exchange

**routing key**

Exchange types:

- direct, `routing key`匹配，消息就被投递到对应的队列
- fanout, 消息会投递给所有附加在此交换器上的队列
- topic, 不同源头的消息到达同一个队列
- headers, 匹配消息的`header`而非`routing key`

指定权限控制在`vhost`or`server` level。

默认情况下无法幸免服务器重启。

**持久化**

- 使用持久化性能受影响
- 持久性消息在RabbitMQ内建集群工作环境下工作的不好

**Transaction**和**发送方确认模式**
