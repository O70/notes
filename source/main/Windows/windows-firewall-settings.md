---
title: Windows Security
date: 2019-04-24 09:00:00
---

# Windows Security

## Firewall & network protection

### Advanced settings

Inbound Rules:
- 1. Select **Port**. Next.
- 2. Specific local ports: `8001-9000`. Next.
- 3. Select **Allow the connection**. Next.
- 4. Select **Domain**, **Private**, **Public**. Next.
- 5. Input **Name** and **Description**. Finish.
