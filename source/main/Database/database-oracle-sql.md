---
title: Oracle SQL
date: 2019-11-05 17:00:00
---

## sqlplus

**Open with Administrator**

> 忘记密码
> https://blog.csdn.net/pucao_cug/article/details/61616717
>``` sh
$ sqlplus /nolog
> connect / as sysdba
> ALTER USER SYSTEM IDENTIFIED BY PTRkygl413;
```

- 以操作系统权限认证的oracle sys管理员登陆
``` sh
$ sqlplus "/as sysdba"
```

- 不在cmd或者terminal当中暴露密码的登陆方式
``` sh
$ sqlplus /nolog
SQL> conn /as sysdba
# or
SQL> conn sys/pwd as sysdba
```

- 非管理员用户登陆
``` sh
$ sqlplus scott/tiger
```

- 非管理员用户使用tns别名登陆
``` sh
$ sqlplus scott/tiger@orcl
```

- 管理员用户使用tns别名登陆
``` sh
$ sqlplus sys/password@orcl as sysdba
```

- 不显露密码的登陆方式
``` sh
$ sqlplus
Enter user-name：sys as sysdba
Enter password：password
```

## Common

- 查看版本
``` sql
SELECT * FROM V$VERSION;

SELECT * FROM PRODUCT_COMPONENT_VERSION;
```

- 查看实例
``` sql
SELECT * FROM V$INSTANCE;
```

- 查看数据库里面所有用户，前提是你是有dba权限的帐号，如sys, system
``` sql
SELECT * FROM DBA_USERS;
```

- 当前用户能管理的所有用户
``` sql
SELECT * FROM ALL_USERS;
```

- 当前用户信息
``` sql
SELECT * FROM USER_USERS;
```

- 查看当前全局数据库名
``` sql
SELECT * FROM GLOBAL_NAME;
```

- 查看用户权限角色
``` sql
SELECT * FROM USER_ROLE_PRIVS;
```

- 查询当前数据库名
``` sql
SELECT * FROM V$DATABASE;
```

- 查看数据库域名
``` sql
SELECT * FROM V$PARAMETER WHERE NAME = 'db_domain';
```

- 查询出当前账号的锁定时间
``` sql
SELECT USERNAME, LOCK_DATE FROM DBA_USERS WHERE USERNAME = 'BID';
```

- 查看当前允许的最大密码输入错误次数
``` sql
SELECT * FROM DBA_PROFILES WHERE RESOURCE_NAME = 'FAILED_LOGIN_ATTEMPTS';
```

- 修改为30次
``` sql
ALTER PROFILE DEFAULT LIMIT FAILED_LOGIN_ATTEMPTS 30;
```

- 修改用户密码与解锁
``` sql
ALTER USER SYS IDENTIFIED BY PTRkygl413;
ALTER USER SYSTEM IDENTIFIED BY PTRkygl413;

ALTER USER BID IDENTIFIED BY BIDqwe567poi_RIPED;
ALTER USER BID ACCOUNT UNLOCK;
```

- 锁与事务
``` sql
SELECT * FROM V$LOCK;

SELECT * FROM V$TRANSACTION;
```

## `expdp/impdp`

> https://www.cnblogs.com/huacw/p/3888807.html
> https://blog.csdn.net/gmaaa123/article/details/82858015

### Export

- 创建目录（仅仅是在oracle里设定的目录，并没有真正创建）
``` sql
CREATE DIRECTORY DIR_BID AS 'D:\\BID_BACK';
-- create or replace directory mydump as 'E:\oracle\dump';
```

``` sql
DROP DIRECTORY DIR_BID;
```

- 授权
``` sql
GRANT READ, WRITE ON DIRECTORY DIR_BID TO BID;
```

- 导出数据，最后不要加分号
``` sh
EXPDP BID/BID_PWD DUMPFILE=BID.DMP DIRECTORY=DIR_BID SCHEMAS=BID

# expdp hrmis/"hrmis"@orcl schemas=hrmis dumpfile=hrmis167.dmp directory=dir_hrmis logfile=hrmis167.log
```

### Import

- 创建目录
``` sql
CREATE DIRECTORY DIR_BID AS 'D:\\BID_BACK';
```

- 创建表空间
``` sql
CREATE TABLESPACE BID 
	DATAFILE 'D:\\app\\BSH\\oradata\\orcl\\BID.DBF' 
	SIZE 100M AUTOEXTEND ON NEXT 100M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 200K;
```

- 创建用户
``` sql
CREATE USER BID IDENTIFIED BY BID_PWD DEFAULT TABLESPACE BID TEMPORARY TABLESPACE TEMP;
```

- 授权
``` sql
GRANT READ, WRITE ON DIRECTORY DIR_BID TO BID;
GRANT CONNECT, RESOURCE, DBA TO BID;
```

- 还原
``` sh
IMPDP BID/BID_PWD DIRECTORY=DIR_BID DUMPFILE=BID.DMP LOGFILE=BID_DATA.LOG SCHEMAS=BID

# IMPDP SYSTEM/ORACLE@ORCL DIRECTORY=DBDATA DUMPFILE=AHNGRM.DMP LOGFILE=AHNGRM.LOG SCHEMAS=AHNGRM
```
