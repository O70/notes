---
title: Tomcat Configure SSL
date: 2019-05-29 17:09:00
---

[](../Java/java-keytool.html)

## Configuration

```xml
<Connector port="8443"
      protocol="org.apache.coyote.http11.Http11Protocol"
      SSLEnabled="true"
      maxThreads="150"
      scheme="https"
      secure="true"
      clientAuth="false"
      sslProtocol="TLS"
      keystoreFile=".\ca\tomcat.keystore"
      keystorePass="123456"
      truststoreFile=".\ca\tomcat.keystore"
      truststorePass="123456" />
```

`clientAuth`指定是否需要验证客户端证书:

- `false`: 单向SSL验证，SSL配置可到此结束
- `true`: 强制双向SSL验证，**必须验证客户端证书**
- `want`: 可以验证客户端证书，但如果客户端没有有效证书，也不强制验证

若为**Tomcat6**,则需要将`protocol`设置为`HTTP/1.1`。

## Test

在**本地(客户端)** `host`文件中添加以下内容:
```
# tomcat服务器IP 生成证书时编造的域名
10.122.163.75 hanzo.com.cn
```

然后访问`https://hanzo.com.cn:8443`。若出现tomcat主页面，则配置无误。

然后将`clientAuth`设置为`true`, 启用强制双向SSL验证。
