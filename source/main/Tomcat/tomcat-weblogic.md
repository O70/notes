---
title: Weblogic
date: 2019-09-08 17:09:00
---

## Building `weblogic-maven-plugin`

参考文档：
> https://docs.oracle.com/middleware/12213/wls/WLPRG/maven.htm#WLPRG587

> https://docs.oracle.com/middleware/1213/core/MAVEN/config_maven.htm#MAVEN8853

**Weblogic version: 12.2.1.3.0**

如果本地没安装Weblogic，可以把服务器上的Weblogic安装目录拷贝至本地。

### 1. Environment Variables

- `ORACLE_HOME`: `INSTALL_DIR\Oracle\Middleware\Oracle_Home`
- `MAVEN_HOME`: `%ORACLE_HOME%\oracle_common\modules\org.apache.maven_3.2.5\bin%\oracle_common\modules\org.apache.maven_3.2.5`

### 2. settings.xml

局域网需要配置代理。

```xml
<settings>
 <!-- Optional -->
 <localRepository>INSTALL_DIR\.m2\repository</localRepository>

 <!-- Required -->
 <servers>
  <server>
   <id>local-mixed</id>
   <username>pms</username>
   <password>mavenpms</password>
     </server>
 </servers>

 <!-- Optional -->
 <mirrors>
  <mirror>
   <id>aliyun</id>
   <mirrorOf>central</mirrorOf>
   <name>Nexus of Aliyun</name>
   <url>http://maven.aliyun.com/nexus/content/groups/public</url>
     </mirror>
    </mirrors>

    <!-- Required -->
    <profiles>
     <profile>
   <id>local-private-repo</id>
   <repositories>
    <repository>
     <id>local-mixed</id>
     <!-- Nexus maven-pms: Allow redeploy -->
     <url>http://10.122.163.216:8081/repository/maven-pms/</url>
    </repository>
   </repositories>
     </profile>
    </profiles>

    <!-- Required -->
 <activeProfiles>
  <activeProfile>local-private-repo</activeProfile>
 </activeProfiles>
</settings>
```

### 3. Building

```
cd %ORACLE_HOME%\oracle_common\plugins\maven\com\oracle\maven\oracle-maven-sync\12.2.1

mvn install:install-file -DpomFile=oracle-maven-sync-12.2.1.pom -Dfile=oracle-maven-sync-12.2.1.jar

mvn com.oracle.maven:oracle-maven-sync:push -DoracleHome=%ORACLE_HOME% -DserverId=local-mixed
```

执行时间略长:
```
[INFO] SUMMARY
[INFO] ------------------------------------------------------------------------
[INFO] PUSH SUMMARY - ARTIFACTS PROCESSED SUCCESSFULLY
[INFO] ------------------------------------------------------------------------
[INFO] Number of artifacts pushed: 1205
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] PUSH SUMMARY - ERRORS ENCOUNTERED
[INFO] ------------------------------------------------------------------------
[INFO] No issues encountered.
[INFO]
[INFO] IMPORTANT NOTE
[INFO] This operation may have added/updated archetypes in your repository.
[INFO] You may need to perform additional manual steps to update
[INFO] your remote archetype catalog.  Check the repository manager's
[INFO] user guide for more information.
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 35:25 min
[INFO] Finished at: 2019-09-18T14:05:38+08:00
[INFO] Final Memory: 12M/125M
[INFO] ------------------------------------------------------------------------
```

### 4. Usage

**建议还原为原来的Maven**

`pom.xml`:
```xml
<plugin>
    <groupId>com.oracle.weblogic</groupId>
    <artifactId>weblogic-maven-plugin</artifactId>
    <version>12.2.1-3-0</version>
</plugin>
```

--------------------------------------------------------------------------------

TODO: 待整理

## On Mac

参考文档:

> https://docs.oracle.com/middleware/12213/wls/WLPRG/maven.htm#WLPRG587

> https://docs.oracle.com/middleware/1213/core/MAVEN/config_maven.htm#MAVEN8853

### 配置环境变量

add ORACLE_HOME
update MAVEN_HOME

修改复制而来的weblogic安装目录权限:
chmod -R 755 Oracle

### Update settings.xml

ORACLE_HOME/oracle_common/modules/org.apache.maven_relnum

配置阿里云云效仓库: https://repomanage.rdc.aliyun.com/my/repo#guide

### Start Build

cd ORACLE_HOME/oracle_common/plugins/maven/com/oracle/maven/oracle-maven-sync/12.2.1
cd $ORACLE_HOME/oracle_common/plugins/maven/com/oracle/maven/oracle-maven-sync/12.2.1
cd %ORACLE_HOME%/oracle_common/plugins/maven/com/oracle/maven/oracle-maven-sync/12.2.1

mvn install:install-file -DpomFile=oracle-maven-sync-12.2.1.pom -Dfile=oracle-maven-sync-12.2.1.jar

mvn com.oracle.maven:oracle-maven-sync:help

mvn com.oracle.maven:oracle-maven-sync:push -DoracleHome=/Users/Guiwang/Workspace/Utility/weblogic/Oracle/Middleware/Oracle_Home
mvn com.oracle.maven:oracle-maven-sync:push -DoracleHome=$ORACLE_HOME -DserverId=rdc-releases

[ERROR] Failed to execute goal com.oracle.maven:oracle-maven-sync:12.2.1-3-0:push (default-cli) on
project standalone-pom: Synchronization execution failed: SyncException:
Location "/Users/Guiwang/Workspace/Utility/weblogic/Oracle/Middleware/Oracle_Home/wlserver\modules\weblogic-L10N_ko.jar"
does not exist -> [Help 1]

默认推送至: https://repo.maven.apache.org/maven2

mvn help:describe -DgroupId=com.oracle.weblogic -DartifactId=weblogic-maven-plugin -Dversion=12.2.1-0-0


IMPORTANT NOTE
This operation may have added/updated archetypes in your repository.
To update your archetype catalog, you should run:
mvn archetype:crawl -Dcatalog=$HOME/.m2/archetype-catalog.xml

e.g.:
mvn archetype:crawl -Dcatalog=/Users/Guiwang/Workspace/Utility/weblogic/.m2/archetype-catalog.xml

## On Windows 10

Building

cd %ORACLE_HOME%\oracle_common\plugins\maven\com\oracle\maven\oracle-maven-sync\12.2.1

**mvn install:install-file -DpomFile=oracle-maven-sync-12.2.1.pom -Dfile=oracle-maven-sync-12.2.1.jar**

mvn com.oracle.maven:oracle-maven-sync:push -DoracleHome=%ORACLE_HOME% -DserverId=rdc-releases -DoverwriteParent=true -DpushDuplicates=true

**mvn com.oracle.maven:oracle-maven-sync:push -DoracleHome=%ORACLE_HOME% -DserverId=local-releases**

mvn help:describe -DgroupId=com.oracle.weblogic -DartifactId=weblogic-maven-plugin -Dversion=12.2.1-0-0

http://10.122.163.216:8081/repository/maven-pms/

java -jar migrate-local-repo-tool.jar -cd .m2/repository -t http://10.122.163.216:8081/repository/maven-pms/ -u pms -p mavenpms

```xml
<plugin>
    <groupId>com.oracle.weblogic</groupId>
    <artifactId>weblogic-maven-plugin</artifactId>
    <version>12.2.1-3-0</version>
    <configuration>
        <adminurl>t3://10.27.213.181</adminurl>
        <user>weblogic</user>
        <password>PTRkygl@ljf</password>
        <source>${project.build.directory}/${project.build.finalName}.${project.packaging}</source>
        <target>AdminServer</target>
        <verbose>true</verbose>
        <name>${project.build.finalName}</name>
    </configuration>
</plugin>
```
