---
title: Tomcat Deploy
date: 2019-11-14 12:25:00
---

## Tomcat7 `server.xml`

控制台中文乱码，`logging.properties`：
``` properties
java.util.logging.ConsoleHandler.encoding = GBK
```

中文乱码：
``` xml
<Connector port="8080" protocol="HTTP/1.1"
	connectionTimeout="20000"
	redirectPort="8443"
	URIEncoding="UTF-8" />
```

Tomcat8启动项目后，浏览器(Firefox/Chrome)JS文件中文乱码(包括注释)。进行以下修改：
`bin/catalina.bat`(Line: `211`):
```
set "JAVA_OPTS=%JAVA_OPTS% %JSSE_OPTS%"
```
to:
```
set "JAVA_OPTS=%JAVA_OPTS% %JSSE_OPTS% -Dfile.encoding=UTF-8"
```

``` xml
<Host name="localhost"  appBase="webapps"
    unpackWARs="true" autoDeploy="true">

	<Context path="" docBase="HRMIS" debug="0" reloadable="true"/>       

</Host>
```

## Tomcat7 `tomcat-users.xml`

``` xml
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
	<role rolename="admin-gui"/>
	<role rolename="admin-script"/>
	<role rolename="manager-gui"/>
	<role rolename="manager-script"/>
	<role rolename="manager-jmx"/>
	<role rolename="manager-status"/>
	<user username="admin" password="admin" roles="manager-gui,manager-script,manager-jmx,manager-status,admin-script,admin-gui"/>
</tomcat-users>
```

对于Tomcat8，默认只能本机访问管理项目。
需要修改`host-manager`和`manager`项目下的`/META-INF/context.xml`:
``` xml
<Valve className="org.apache.catalina.valves.RemoteAddrValve"
     allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
```
改为
``` xml
<Valve className="org.apache.catalina.valves.RemoteAddrValve"
     allow="\d+\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
```

## `pom.xml`

``` xml
<plugin>
	<groupId>org.apache.tomcat.maven</groupId>
	<artifactId>tomcat7-maven-plugin</artifactId>
	<version>2.2</version>
	<configuration>
		<url>http://10.122.163.76:8080/manager/text</url>
		<path>/${projectName}</path>
		<username>admin</username>
		<password>admin</password>
	</configuration>
</plugin>
```

or

``` xml
<plugin>
	<groupId>org.apache.tomcat.maven</groupId>
	<artifactId>tomcat7-maven-plugin</artifactId>
	<version>2.2</version>
	<configuration>
		<url>http://10.122.163.76:8080/manager/text</url>
		<path>/${projectName}</path>
		<server>tomcat7</server>
	</configuration>
</plugin>
```

`settings.xml`:
``` xml
<servers>
	<server>  
		<id>tomcat7</id>  
		<username>admin</username>  
		<password>admin</password>  
	</server>  
</servers>
```
