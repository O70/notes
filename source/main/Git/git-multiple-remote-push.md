---
title: Multiple Remote Push
date: 2019-06-17 12:32:00
---

## One By One to Push

- Adds Remote
```sh
$ git remote add github git@github.com:O70/Notes.git
$ git remote add gitee git@gitee.com:Guiwang/Notes.git
$ git remote add gitlab git@gitlab.com:O70/Notes.git
$ git remote add bitbucket git@bitbucket.org:O70/Notes.git

$ git push -u github master
```

- `.git/config`
```
[remote "github"]
	url = git@github.com:O70/Notes.git
	fetch = +refs/heads/*:refs/remotes/github/*
[branch "master"]
	remote = github
	merge = refs/heads/master
[remote "gitee"]
	url = git@gitee.com:Guiwang/Notes.git
	fetch = +refs/heads/*:refs/remotes/gitee/*
[remote "gitlab"]
	url = git@gitlab.com:O70/notes.git
	fetch = +refs/heads/*:refs/remotes/gitlab/*
[remote "bitbucket"]
	url = git@bitbucket.org:O70/notes.git
	fetch = +refs/heads/*:refs/remotes/bitbucket/*
```

- Pull
```sh
$ git pull github master
#!! or
$ git pull gitee master
#!! or
$ git pull gitlab master
#!! or
$ git pull bitbucket master
```

- Push
```sh
$ git push github master
$ git push gitee master
$ git push gitlab master
$ git push bitbucket master

#!! or
$ git push github master ; git push gitee master ; git push gitlab master ; git push bitbucket master
```

## One-time Push

- Clear `.git/config` Remote
```sh
$ git remote remove github
$ git remote remove gitee
$ git remote remove gitlab
$ git remote remove bitbucket

#!! or
$ git remote remove github ; git remote remove gitee ; git remote remove gitlab ; git remote remove bitbucket
```

- Add `origin`
```sh
$ git remote add origin git@github.com:O70/Notes.git
$ git push -u origin master
```

> **Note**
>
> **`origin`作为主`upstream`, 主要用于`git pull`**

- Adds Remote
```sh
$ git remote add mp git@github.com:O70/Notes.git
$ git remote set-url --add mp git@gitee.com:Guiwang/Notes.git
$ git remote set-url --add mp git@gitlab.com:O70/notes.git
$ git remote set-url --add mp git@bitbucket.org:O70/notes.git
```

- `.git/config`
```
[remote "origin"]
	url = git@github.com:O70/Notes.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
[remote "mp"]
	url = git@github.com:O70/Notes.git
	fetch = +refs/heads/*:refs/remotes/mp/*
	url = git@gitee.com:Guiwang/Notes.git
	url = git@gitlab.com:O70/notes.git
	url = git@bitbucket.org:O70/notes.git
```

- Pull
```sh
$ git pull origin master
#!! or
$ git pull
```

- Push
```sh
$ git push mp master
```

> Push结束后, 会出现下图问题:
>
> ![](./imgs/git-0001.png)

- Sync `origin` and `mp`
```sh
$ git remote update
```

## One-time Push for `origin`

- Remove `mp`
```sh
$ git remote remove mp
```

- Adds Remote
```sh
$ git remote set-url --add origin git@gitee.com:Guiwang/Notes.git
$ git remote set-url --add origin git@gitlab.com:O70/notes.git
$ git remote set-url --add origin git@bitbucket.org:O70/notes.git

#!! or
$ git remote set-url --add origin git@gitee.com:Guiwang/Notes.git ; git remote set-url --add origin git@gitlab.com:O70/notes.git ; git remote set-url --add origin git@bitbucket.org:O70/notes.git
```

- Delete Remote
```sh
$ git remote set-url --delete origin git@gitee.com:Guiwang/Notes.git
$ git remote set-url --delete origin git@gitlab.com:O70/notes.git
$ git remote set-url --delete origin git@bitbucket.org:O70/notes.git
```

- `.git/config`
```
[remote "origin"]
	url = git@github.com:O70/Notes.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	url = git@gitee.com:Guiwang/Notes.git
	url = git@gitlab.com:O70/notes.git
	url = git@bitbucket.org:O70/notes.git
[branch "master"]
	remote = origin
	merge = refs/heads/master
```

- Pull
```sh
$ git pull origin master
```

> **Note**
>
> **`git pull`有可能会出问题, 只会`pull`第一个`url`的, 而且可能还会冲突**

- Push
```sh
$ git push origin master
```
