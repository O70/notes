---
title: Git Flow
date: 2018-01-21 09:00:00
---

# Git Flow

__约定:__

> * 任何开发人员只能通过变基提交向`master`分支推送代码
> * `master`分支每个提交必需打上标签(`Tag`)
> * 测试/产品部署基于`dev`分支
> * 新功能(模块)分支基于`dev`分支，命名为`feature/MODULE_NAME`
> * Bug分支给予`dev`分支，命名为`bug/BUG_NAME`

### 基本范例

__使用不同的`Git`命令组合或`GitLab`操作往往可以达到相同目的。__

当前范例从新建项目开始，已存在的项目相关操作也在其中。

#### 1.  创建项目并建立`dev`分支

在`GitLab`上创建项目(`git-notes`)。
直接在`GitLab`上添加`README.md`文件，创建`master`分支的第一个提交`Initial commit`。

创建`dev`分支：

```
git clone REPO_URL/git-notes
git checkout -b dev #^创建并检出dev分支
git branch -a #^查看所有分支
git push -u origin dev #^新建远程追踪分支，保持本地与远程可同步
```

当然可以直接在`GitLab`上新建`dev`分支，简单而直接。在之后的克隆可以如下操作：

```
git clone REPO_URL/git-notes
git checkout -b dev origin/dev #^建立并启动本地分支dev，用于追踪远程分支origin/dev
```

#### 2. 开发新功能

e.g.: A和B开始开发新功能

A:
```
git checkout -b feature/admin dev
git status
git add .
git commit -m 'feat(admin): 添加基本服務'

git checkout dev
git pull

git merge feature/admin #^合并本地功能分支feature/admin至本地dev分支
git push
```

B:
```
git checkout -b feature/admin_ui dev
git status
git add .
git commit -m 'feat(Admin-UI): 添加後台管理UI'

git checkout dev
git pull

git merge feature/admin_ui
git push
```

__关于提交消息请阅读[Commit message 和 Change log 编写指南](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)__

操作到现在，功能分支仅存在于本地。依据目前规则，测试/产品部署基于`dev`分支代码，所以不是所有功能分支的代码都能够及时合并到本地`dev`分支，并推送至远程`origin/dev`分支上。但本地功能分支代码又不能仅存放于本地，可将本地功能分支推送至`GitLab`，如下操作，以A为例：

```
git checkout feature/admin
git push -u origin feature/admin
```

远程仓库建立分支`feature/admin`，与本地分支`feature/admin`相对应，往后的操作(pull/push)都可以一直基于此分支。
当然如果需要合并代码至`dev`分支，还是和上述操作一样。

在功能开发完毕之后，所有代码都合并到了`dev`分支上，需要删除无用的功能分支。

```
git push --delete origin feature/admin #^删除远程仓库中的feature/admin分支
git branch --unset-upstream feature/admin #^从.git/config文件中去除相应配置
git branch -d feature/admin #^删除本地feature/admin分支
```

__修改Bug也需要创建分支(`bug/BUG_NAME`)，操作与开发新功能类似。__

#### 3. 合并代码至`master`

测试/产品部署基于`dev`分支，完成之后，合并`dev`分支代码至`master`，并打上`Tag`：

分支开发完成后，很可能有一堆commit，但是合并到主干的时候，往往希望只有一个（或最多两三个）commit，这样不仅清晰，也容易管理。

```
git checkout master
git merge dev
git rebase -i HEAD~2 #^https://git-scm.com/book/zh/v1/Git-工具-重写历史
git tag v0.1
git push
git push origin v0.1
```
