---
title: GitLab Issues
date: 2018-06-21 14:30:00
---

## Issues Labels

> **类型**
- Group Label(推荐)
- Project Label

> **作用**
- 标注`Issue`优先级
- 根据`Label`过滤未关闭的`Issue`
- 根据每个项目的`Board`对`Issue`进行分类

## New Issue

**Issue默认创建在每个微服务的`UI`项目中，e.g.: food-ui, training-ui。**

**创建Issue需要对应相关的微服务。即选中具体项目进行创建Issue**

- Title
- Description
- Assignee
  - 一个人直接在下拉框选中
  - 多个人在 **Description** 中`@mention`, e.g.: @Guiwang @HANZO
- Labels，根据`issue`含义进行添加，可选中多个

## Issues Close

> **Commit Message**
- [Commit message 和 Change log 编写指南](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)
- 每个`commit message`应该尽量清晰明了，仅看行首就知道某次`commit`的目的
- 根据`commit`生成 **Change log**

> **Close**
- git commit -m 'close #3'
- git commit -m 'msg. close #3'
- git commit -m 'closes #2, #3, #6'
- git commit -m 'msg. closes #2, #3, #6'

**也可手动在GitLab中直接关闭**
