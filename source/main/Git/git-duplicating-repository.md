---
title: Duplicating a repository
date: 2019-06-16 20:28:00
---

[Duplicating a repository](https://help.github.com/en/articles/duplicating-a-repository#mirroring-a-repository-that-contains-git-large-file-storage-objects)

To duplicate a repository without forking it, you can run a special clone command, then mirror-push to the new repository.

Before you can duplicate a repository and push to your new copy, or mirror, of the repository, you must create the new repository on GitHub. In these examples, `exampleuser/new-repository` or `exampleuser/mirrored` are the mirrors.

## Mirroring a repository

1. Open Terminal.

2. Create a bare clone of the repository.
```sh
$ git clone --bare https://github.com/exampleuser/old-repository.git
```

3. Mirror-push to the new repository.
```sh
$ cd old-repository.git
$ git push --mirror https://github.com/exampleuser/new-repository.git
```

4. Remove the temporary local repository you created in step 1.
```sh
$ cd ..
$ rm -rf old-repository.git
```
## Mirroring a repository that contains Git Large File Storage objects
