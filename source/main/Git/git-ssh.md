---
title: Git SSH
date: 2019-06-10 10:30:00
---

## Add the SSH key to your GitHub/Gitee account

- Generating a new SSH key
```sh
$ ssh-keygen -t rsa -b 4096 -C "thraex@aliyun.com"
# file name is mac-thraex
```

- Copy the SSH key to your clipboard
```sh
$ cat id_rsa.pub
# ...
```

- Test
```sh
$ ssh -T git@github.com
# or
$ ssh -T git@gitee.com
```
