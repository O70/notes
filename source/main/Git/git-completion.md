---
title: Git Completion
date: 2019-06-09 17:53:00
---

## Mac

```sh
$ brew list
$ brew install bash-completion
$ brew info bash-completion 
```

- Download
```sh
$ curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash
```

- Configuration
```sh
$ vim .bash_profile
...
# add .git-completion.bash
source ~/.git-completion.bash
...
```

- source
```sh
$ source ~/.bash_profile
```
