---
title: git-svn
date: 2019-05-21 09:00:00
---

# git-svn

## clone

```
git svn clone http://svn.example.com/project/trunk
```

e.g.:
```
git svn clone svn://<address>/SourceCode/trunk/<project_name>
```

```
git stash
git rebase -i HEAD~2
git checkout master
git merge dev
git push # git push origin master
git checkout dev
git stash pop stash@{0}
```
