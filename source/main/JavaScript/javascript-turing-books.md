---
title: Turing Books
date: 2018-07-09 18:45:00
---

## Sample Book Exchange

- [ ] 黑客与画家：硅谷创业之父Paul Graham文集
- [ ] Linux程序设计(第4版)
- [x] MySQL必知必会
- [ ] 信息简史
- [ ] JavaScript高级程序设计（第3版）
- [ ] 深入浅出Node.js
- [ ] 你不知道的JavaScript（中卷）
- [ ] 精通Git（第2版）
- [ ] 你不知道的JavaScript（下卷）
- [ ] 算法图解
- [ ] Java编程思维
- [ ] Tomcat架构解析
- [ ] Node.js实战（第2版）
- [x] OAuth 2实战
- [ ] JSON实战
- [ ] shell脚本实战（第2版）
