---
title: Web Outline
date: 2019-12-05 14:00:00
---

## Web Type

**Static Web**/**Dynamic Web**

> [静态web和动态web的区别与联系](https://blog.csdn.net/mooreliu/article/details/45563227)

**Java Applet**: 一种Java程序。它一般运行在支持Java的Web浏览器内。因为它有完整的Java API支持, 所以Applet是一个全功能的Java应用程序。

**Java Servlet**: 创建基于Web的应用程序提供了基于组件、独立于平台的方法，可以不受CGI程序的性能限制。Servlet有权限访问所有的Java API，包括访问企业级数据库的JDBC API。

> [Java Servlet与Applet、CGI、JSP的比较](https://www.nowcoder.com/questionTerminal/95fb71af5b9c44a0981675a21a3f01e2)
> [Applet和Servlet有什么区别？](https://blog.51cto.com/wushank/1678280)

Java Web/.NET/PHP/Node.js Web/Python Web

CS/BS
