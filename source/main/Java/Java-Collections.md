---
layout: post
title: Java Collections
date: 2018-12-17 10:03:13
categories:
- Technology
- Java
tags:
- Collections
---

# Java Collections

The initial release of Java supplied only a small set of classes for the most useful data structures: **Vector**, **Stack**, **Hashtable**, **BitSet**, and the **Enumeration** interface
