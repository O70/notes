---
title: JVM Tool
date: 2019-07-18 09:54:00
---

## Thread State

| State | Description |
| ----- | -------- |
| __`new`__ | 新建(时间很短) |
| __`runnable`__ | 运行 |
| __`waiting`__/__`timed waiting`__ | 无限期等待/限期等待 |
| __`blocked`__ | 阻塞 |
| __`terminated`__ | 结束(时间很短) |

## Command Line Tools

| Name | Description |
| ---- | ----------- |
| __`jps`__ | Java Virtual Machine Process Status Tool |
| __`jstat`__ | Java Virtual Machine Statistics Monitoring Tool |
| __`jstatd`__ | Virtual Machine `jstat` Daemon |
| __`jinfo`__ | Configuration Info |
| __`jmap`__ | Memory Map |
| __`jhat`__ | Java Heap Analysis Tool |
| __`jstack`__ | Stack Trace |
| __`jdb`__ | Java debugger |
| __`jcmd`__ | None |

- `jps`
```
-q: 只输出进程ID
-m: 输出传入main方法的参数
-l: 输出完全的包名，应用主类名，jar的完全路径名
-v: 输出jvm参数
-V: 输出通过flag文件传递到JVM中的参数
```

- `jstat`

- `jstatd`

- `jinfo`

- `jmap`

- `jhat`

- `jstack`

- `jdb`

- `jcmd`

## Visualization Tool

| Name | Description |
| ---- | ----------- |
| __`jconsole`__ | None |
| __`jvisualvm`__ | None |
