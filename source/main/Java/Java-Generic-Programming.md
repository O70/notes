---
layout: post
title: Java Generic Programming
date: 2018-12-17 10:03:13
categories:
- Technology
- Java
tags:
- Generic
---

# Generic Programming

- type parameters
- type variable

## Generic Class

The Java library uses the variable **E** for the element type of a collection, **K** and **V** for key and value types of a table, and **T** (and the neighboring letters **U** and **S**, if necessary) for “any type at all”.

```java
public class Pair<T> {

    private T first;
    private T second;

    public Pair() {
        this.first = null;
        this.second = null;
    }

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }
}
```

```java
public class Pair<T, U> {
  ...
}
```

## Generic Method

You can define **generic methods** both inside ordinary classes and inside generic classes.
**泛型方法** 可以定义在普通类中，也可以定义在泛型类中。

```java
public class ArrayAlg {

    public static <T> T getMiddle(T... a) {
        return a[a.length/2];
    }

    public static <T extends Comparable> T min(T[] a) {
        return null;
    }

}
```

## Generic Code and the Virtual Machine

The virtual machine does not have objects of generic types—all objects belong to ordinary classes.
虚拟机没有泛型类型对象——所有对象都属于普通类。
