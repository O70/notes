---
title: keytool
date: 2019-05-29 16:00:00
---

## 生成**keystore**文件

```
keytool -genkey -alias tomcat -keyalg RSA -keysize 1024 -keypass 123456 -validity 3650 -keystore C:\Workspace\Kits\apache-tomcat\conf\ca\tomcat.keystore -storepass 123456
```

```
keytool -genkey -alias tomcat -keyalg RSA -keysize 1024 -dname "CN=hanzo.com.cn,OU=bj,S=bj,C=CN" -keypass 123456 -validity 3650 -keystore C:\Workspace\Kits\apache-tomcat\conf\ca\tomcat.keystore -storepass 123456
```

## 生成**pem**文件

```
keytool -certreq -alias tomcat -file C:\Workspace\Kits\apache-tomcat\conf\ca\cert_req.pem -keypass 123456 -storepass 123456 -keystore C:\Workspace\Kits\apache-tomcat\conf\ca\tomcat.keystore
```

## import

```
keytool -import -alias WIN-DSS3B9RGCMP -keystore cacerts -file C:\ca\WIN-DSS3B9RGCMP.crt
```

```
keytool -list -keystore cacerts -alias WIN-DSS3B9RGCMP
```

```
keytool -delete -alias WIN-DSS3B9RGCMP -keystore cacerts
```
