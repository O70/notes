---
title: Common Site
date: 2019-06-22 16:34:00
---

## List

- [GitHub](https://github.com)

- [Gitee](https://gitee.com)

- [GitLab](https://gitlab.com)

- [Bitbucket](https://bitbucket.org)

- [Aliyun](https://homenew.console.aliyun.com)

- [AliMail](https://mail.aliyun.com)

- [AliMail for Enterprise](https://qiye.aliyun.com)

- [Tencent Cloud](https://console.cloud.tencent.com)

- [Exmail](https://exmail.qq.com/cgi-bin/bizmail?sid=WeLTX-sXZryxMBtG,7&t=qy_rf_home&init=1&r=0.801142286357726)

- [Coding](https://dev.tencent.com/user)

- [Coding for Enterprise](https://thraex.coding.net/user)
