# Java

- Oak
- Applet
- JVM
- Bootstrap ClassLoader
- Extension ClassLoader
- I/O, Input/Output
- O/R Mapping, Object-Relational Mapping
- JDBC, Java Database Connectivity
- Connection
- Statement
- EJB, Enterprise Java Beans
- Hibernate(Gavin King, 2001)
- iBatis(2001)
- Spring(Rod Johnson, 2004)
- JPA, Java Persistence API
- TCP/IP
- Socket

